//Function to check who wins rock paper scissors game
func checkWinner(thingA: String, thingB: String) {
   
    //Paper (position 1), Rock (position 2), Scissors (position 3)
    let gameElements = ["Rock", "Paper", "Scissors"]
    var winner: String
   
    //Check if given elements are in the game array and if so then get indexes of game elements
    guard let elementA = gameElements.firstIndex(of: thingA), let elementB = gameElements.firstIndex(of: thingB) else {
        print("\(thingA) VS \(thingB) and there is no winner - wrong elements")
        return
    }
    
    //Check if draw
    if elementA == elementB {
        winner = "there is no winner - draw"
    } else {
        //Check if element A is one place right to B, or A is the first element in array and B is the last
        if (elementA - elementB == 1) || (elementA == 0 && elementB == gameElements.count - 1) {
            winner = "\(thingA)"
        } else {
            winner = "\(thingB)"
        }
    }
    print( "\(thingA) VS \(thingB) and winner is : \(winner)")
}

checkWinner(thingA: "Scissors", thingB: "Paper")
checkWinner(thingA: "Paper", thingB: "Scissors")
checkWinner(thingA: "Rock", thingB: "Scissors")
checkWinner(thingA: "Scissors", thingB: "Rock")
checkWinner(thingA: "Rock", thingB: "Paper")
checkWinner(thingA: "Paper", thingB: "Rock")
checkWinner(thingA: "Metal", thingB: "Paper")
checkWinner(thingA: "Rock", thingB: "Metal")
checkWinner(thingA: "Metal", thingB: "Metal")
checkWinner(thingA: "Rock", thingB: "Rock")
checkWinner(thingA: "Paper", thingB: "Paper")
checkWinner(thingA: "Scissors", thingB: "Scissors")
